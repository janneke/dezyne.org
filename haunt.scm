;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;; Copyright © 2018, 2019, 2021, 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of dezyne.org.
;;;
;;; dezyne.org is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; dezyne.org is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with dezyne.org.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; The text and images on this site are free culture works available
;;; under the Creative Commons Attribution-NoDerivs 4.0 license, see
;;; https://creativecommons.org/licenses/by-nd/4.0/

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (haunt reader texinfo)
             (haunt site)
             (haunt utils)
             (commonmark)
             (syntax-highlight)
             (syntax-highlight scheme)
             (syntax-highlight xml)
             (syntax-highlight c)
             (sxml match)
             (sxml transform)
             (texinfo)
             (texinfo html)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 rdelim)
             (ice-9 regex)
             (ice-9 match)
             (web uri))

(define %releases
  '(("2.17.8" #t)))

(define (tarball-name name version)
  (string-append name "-" version ".tar.gz"))

(define (linux-name name version)
  (string-append name "-" version "-x86_64-linux.tar.gz"))

(define (windows-name name version)
  (string-append name "-" version "-x86_64-windows.zip"))

(define (dezyne-download-url name)
  (string-append "/download/dezyne/" name))

(define (dezyne-tarball-name version)
  (string-append (tarball-name "dezyne" version)))

(define (dezyne-linux-name version)
  (string-append (linux-name "dezyne" version)))

(define (dezyne-windows-name version)
  (string-append (windows-name "dezyne" version)))

(define (verum-dezyne-download-url name)
  (string-append "/download/verum-dezyne/" name))

(define (verum-dezyne-linux-name version)
  (string-append (linux-name "verum-dezyne" version)))

(define (verum-dezyne-windows-name version)
  (string-append (windows-name "verum-dezyne" version)))

(define %asd-converter-releases
  '(("0.1.9" #f)))

(define (asd-converter-download-url name)
  (string-append "https://kluit.dezyne.org/download/asd/" name))

(define (asd-converter-linux-name version)
  (string-append "asd" "-" version "-x86_64-linux.gz"))

(define (development-dezyne-download-url name)
  (string-append "https://kluit.dezyne.org/download/dezyne/" name))

(define (development-verum-dezyne-download-url name)
  (string-append "https://kluit.dezyne.org/download/verum-dezyne/" name))

(define (date year month day)
  "Create a SRFI-19 date for the given YEAR, MONTH, DAY"
  (let ((tzoffset (tm:gmtoff (localtime (time-second (current-time))))))
    (make-date 0 0 0 0 day month year tzoffset)))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define* (anchor content #:optional (uri content))
  `(a (@ (href ,uri)) ,content))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

;;Creative Commons Attribution-NoDerivs 4.0 license (a.k.a. CC BY-ND) (#ccbynd)
(define %cc-by-nd-link
  '(a (@ (href "https://creativecommons.org/licenses/by-nd/4.0/"))
      "Creative Commons Attribution-NoDerivs 4.0"))

(define %cc-by-nd-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-nd/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-nd/4.0/80x15.png")))))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define* (centered-image url #:optional alt)
  `(img (@ (class "centered-image")
           (src ,url)
           ,@(if alt
                 `((alt ,alt))
                 '()))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define dezyne.org-theme
  (theme #:name "dezyne.org"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "dezyne.org"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "blog" "/"))
                            (li ,(link "documentation" "/documentation.html"))
                            (li ,(link "download" "/download.html"))
                            (li ,(link "publications" "/publications.html"))
                            (li ,(link "contact" "/contact.html"))
                            (li ,(link "verum.com" "https://verum.com"))
                            (li (@ (class "fade-text")) " ")))
                   ,@(if (not (equal? title "Recent Blog Posts")) '()
                       `((div (@ (class "right-box"))
                              ,(anchor (centered-image "images/feed.png" "atom")
                                       "/feed.xml"))))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2018,2019,2020,2021,2022,2023 <janneke@gnu.org>"
                              ,%cc-by-nd-button)
                           (p "The text and images on this site are
free culture works available under the " ,%cc-by-nd-link " license.")
                           (p "This website is built with "
                              (a (@ (href "http://haunt.dthompson.us"))
                                 "Haunt")
                              ", a static site generator written in "
                              (a (@ (href "https://gnu.org/software/guile"))
                                 "GNU Guile")
                              "."))))))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define* (collections #:key (file-name "index.html"))
  `(("Recent Blog Posts" ,file-name ,posts/reverse-chronological)))

(define parse-lang
  (let ((rx (make-regexp "-*-[ ]+([a-z]*)[ ]+-*-")))
    (lambda (port)
      (let ((line (read-line port)))
        (match:substring (regexp-exec rx line) 1)))))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define (highlight-scheme code)
  `(pre (code ,(highlights->sxml (highlight lex-scheme code)))))

(define (raw-snippet code)
  `(pre (code ,(if (string? code) code (read-string code)))))

;; Markdown doesn't support video, so let's hack around that!  Find
;; <img> tags with a ".webm" source and substitute a <video> tag.
(define (media-hackery . tree)
  (sxml-match tree
    ((img (@ (src ,src) . ,attrs) . ,body)
     (if (string-suffix? ".webm" src)
         `(video (@ (src ,src) (controls "true"),@attrs) ,@body)
         tree))))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img . ,media-hackery)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout dezyne.org-theme site title body)
               sxml->html)))

(define (documentation-page)
  (static-page
   "documentation"
   "documentation.html"
   `((h1 "Dezyne Reference Manual")
     (ul
      (li ,(anchor "HTML, with one page per node" "/dezyne/manual/dezyne/html_node/"))
      (li ,(anchor "HTML, as one big page" "/dezyne/manual/dezyne/dezyne.html"))
      (li ,(anchor "PDF" "/dezyne/manual/dezyne.pdf")))
     (h1 "DEVELOPMENT Dezyne Reference Manual")
     (ul
      (li ,(anchor "HTML, with one page per node" "/devel/manual/dezyne/html_node/"))
      (li ,(anchor "HTML, as one big page" "/devel/manual/dezyne/dezyne.html"))
      (li ,(anchor "PDF" "/devel/manual/dezyne.pdf"))))))

(define (download-page)
  (static-page
   "download"
   "download.html"
   `((h2 "Latest Dezyne downloads")
     (p "See the "
        (a (@ (href "/index.html")) "blog")
        " for the release notes.")
     (p "See the Dezyne Reference Manual: "
        (a (@ (href "/dezyne/manual/dezyne/html_node/Installation.html#Installation")) "Installation")
        " for full instructions.")
     (p "See "
        (a (@ (href "https://download.verum.com")) "download.verum.com")
        " for executable releases.")
     (table (@ (class "table"))
            (thead
             (tr (th (@ (colspan "2")) "Git")))
            (tbody
             (tr
              (td (@ (colspan "2"))
                  "git clone "
                  (a (@ (href "https://git.savannah.nongnu.org/cgit/dezyne.git"))
                     "git://git.savannah.nongnu.org/dezyne"))))
            (thead
             (tr (th "Source tarball") (th "GPG signature")))
            (tbody
             ,(map (match-lambda
                     ((version signature?)
                      (let* ((name (dezyne-tarball-name version))
                             (sig (string-append name ".sig")))
                        `(tr
                          (td (a (@ (href ,(dezyne-download-url name))) ,name))
                          (td ,(if signature?
                                   `(a (@ (href ,(dezyne-download-url sig))) ,sig)
                                   ""))))))
                   %releases)))
     (h3 "Older versions")
     (p "See "
        (a (@ (href ,(dezyne-download-url ""))) "download/dezyne"))

     (h3 "Development releases")
     (p "See "
        (a (@ (href ,(development-dezyne-download-url "")))
           "kluit.dezyne.org/download/dezyne")))))

(define (publications-page)
  (static-page
   "publications"
   "publications.html"
   `((h1 "Publications")
     (ul
      (li ,(anchor "Dezyne: Paving the Way to Practical Formal Software Engineering"
                   "https://arxiv.org/abs/2108.02962")
          (p "Proceedings of the 6th Workshop on Formal Integrated Development Environment, F-IDE 2021.")
          (p "Published Aug 06, 2021 by Rutger van Beusekom, Bert de Jonge, Paul Hoogendijk, Jan Nieuwenhuizen"))
      (li ,(anchor "Formalising the Dezyne Modelling Language in mCRL2"
                   "https://research.tue.nl/nl/publications/formalising-the-dezyne-modelling-language-in-mcrl2")
          (p "Joint 22nd International Workshop on Formal Methods for Industrial Critical Systems and 17th International Workshop on Automated Verification of Critical Systems, FMICS-AVoCS 2017.")
          (p "  Published Sept 18, 2017 by R. van Beusekom, J.F. Groote, P. Hoogendijk, R. Howe, W. Wesselink, R. Wieringa, T.A.C. Willemse."))))))

(define (contact-page)
  (static-page
   "contact"
   "contact.html"
   `((h2 "IRC Channel")
     (p "Join us in the " (code "#dezyne")
        " channel on the "
        ,(anchor "Libera Chat IRC network"
                 "https://web.libera.chat")
        ".")
     (h2 "Development mailing list")
     (p "Subscribe to the Dezyne development mailing list "
        ,(anchor "dezyne-devel"
                 "https://lists.gnu.org/archive/html/dezyne-devel/")
        ".")
     (h2 "Bug reporting")
     (p "If you found a bug, please check if it is already in the "
        ,(anchor "bug database"
                 "https://gitlab.com/dezyne/dezyne-issues/-/issues")
        ".  If not, please report it to: "
        ,(anchor "bug-dezyne@nongnu.org"
                 "mailto:bug-dezyne@nongnu.org")
        "."))))

(site #:title "dezyne.org"
      #:domain "dezyne.org"
      #:default-metadata
      '((author . "Janneke")
        (email  . "janneke@gnu.org"))
      #:readers (list commonmark-reader* texinfo-reader)
      #:builders (list (blog #:theme dezyne.org-theme #:collections (collections))
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (documentation-page)
                       (download-page)
                       (publications-page)
                       (contact-page)
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "videos")
                       (static-directory "src")
                       (static-directory "manuals")))
