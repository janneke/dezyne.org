# dezyne.org -- Haunted Dezyne.
# Copyright © 2018, 2019, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of dezyne.org.
#
# dezyne.org is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# dezyne.org is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dezyne.org.  If not, see <http://www.gnu.org/licenses/>.

SITE:=dezyne
CONFIG:=$(SITE).scm
PORT=8889
$(info CONFIG=$(CONFIG))

.PHONY: all clean default haunt serve gnu-dot-org

default: all

all: haunt

clean:
	git clean -fdx

haunt:
	haunt build --config=$(CONFIG)

site/index.html:
	$(MAKE) haunt

serve: site/index.html
	haunt serve --config=$(CONFIG) --watch --port=$(PORT)

publish: site/index.html
	rsync -avzz site/ kluit.dezyne.org:/srv/dezyne.org/site/

save-doc:
	rsync -avz site/dezyne site/devel site-doc/

restore-doc:
	rsync -avz site-doc/dezyne site-doc/devel site/
