title: Dezyne 2.16.2 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-10-17 10:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.16.2-released
summary: dezyne/ANNOUNCE-2.16.2
---
Dezyne 2.16.2 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.16.2.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.16.2.tar.gz)  
    [dezyne-2.16.2.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.16.2.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    b90911cca1af7345096607717d9314c2bc7d41a0  dezyne-2.16.2.tar.gz  
    f3e84a9c1629088d1e00e01cbd5cfa3ff3ce5b216c4789c7ebeada142547d036  dezyne-2.16.2.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.16.2 since 2.16.1
  * Language
    - In a valued function, after an "illegal" a return is no longer
      required.
    - Shadowing of a type name with a variable identifier is now a
      well-formedness error.
    - Compound namespace definitions are now supported.
  * Noteworthy bug fixes
    - A bug in the C++ and C# runtime has been fixed that could lead to
      reading from an empty list of timers when using defer.
    - The C++ code generator no longer generates multiple includes when
      multiple interfaces are defined in one file ([#75](https://gitlab.com/dezyne/dezyne-issues/-/issues/75)).
    - The simulator now produces a correct split-arrow trace for certain
      models with a V-fork compliance error.
    - A crash in normalization has been fixed for certain models with
      non-deterministic guards.
    - The well-formedness check now gracefully handles an action with an
      undefined type, used in an expression, and report the error.
    - The parser no longer crashes when using a comment on an enum field.
    - The `inline-functions' transformation now correctly handles the
      optional list of function names and also transforms function bodies.
    - A system diagram can now also be generated for some incomplete
      (faulty) systems.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
