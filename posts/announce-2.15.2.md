title: Dezyne 2.15.2 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-06-20 16:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.15.2-released
summary: dezyne/ANNOUNCE-2.15.2
---
Dezyne 2.15.2 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.15.2.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.15.2.tar.gz)  
    [dezyne-2.15.2.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.15.2.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    6cf3b03c0a9576d5e7b6230a7ebec97dc3e8f542 dezyne-2.15.2.tar.gz  
    cee0e844c753f830b78c0ef1d1824aab5a4758f47dd7c096409f70d86f65c47b  dezyne-2.15.2.tar.gz
    
[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the 'gpg --verify' command.

# NEWS

### Changes in 2.15.2 since 2.15.1
  * Build
    - The test suite now passes on i686-linux.
  * Code
    - The c++ runtime now avoids making unnecessary copies.
    - The c++ runtime now avoids construction of trace strings when
      tracing is disabled.
    - The experimental c, javascript, and scheme code generators now pass
      the test suite for their partial supported feature set.
  * Noteworthy bug fixes
    - The parser now accepts `on <event>, inevitable:` and `on <event>,
      optional:`.  This was a regression since 2.9.1.
    - The `dzn graph` command now also honors the -m,--model option for
      --format=json.
    - The `dzn graph --backend=state` command now produces sorted output.
    - The simulator no longer changes the state of the provides port
      without showing provides port interaction when using
      `-C,--no-compliance`.
    - The order of `-I,--import` options is now preserved.
    - The dzn code generator has been fixed by:
      + Adding spacing for port qualifiers,
      + Adding support for `blocking` port qualifier,
      + Preserving data type definitions,
      + Preserving the scope of of function types,
      + Including import statements.
    - A bug was fixed in the experimental c code generator with respect to
      negation in expressions.
    - The experimental javascript and scheme code generators now support
      reply on a synchronous out event.
    - The experimental c, javascript and scheme code generators now use a
      colon (`:`) as separator between the enum-type and enum-field, in
      compliance with this change in 2.13.0.
    - The experimental scheme code generator now supports non-collateral
      blocking, i.e., exclusively in the top component of a system.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like IDE support based on the LSP (Language Server Protocol),
interactive integrated graphics, interactive simulation, (custom) code
generation and (custom) runtime library support.
