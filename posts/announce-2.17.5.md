title: Dezyne 2.17.5 released.
author: Rutger van Beusekom, Janneke Nieuwenhuizen
date: 2023-08-11 18:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.17.5-released
summary: dezyne/ANNOUNCE-2.17.5
---
Dezyne 2.17.5 is a bug-fix release.
Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.17.5.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.17.5.tar.gz)  
    [dezyne-2.17.5.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.17.5.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    f0bdcf1314da1ea002f51849b141177d4701aac5  dezyne-2.17.5.tar.gz  
    ef31e3f0062295b220cc294286dc9ffd0ce4c59fd4b8bbeda7162be04e885096  dezyne-2.17.5.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne

# NEWS
### Changes in 2.17.5 since 2.17.4
  * Noteworthy bug fixes
    - A regression introduced in 2.17.4 was fixed.  It was a side-effect
      of the lookup name performance fix that removed quadratic behavior.
    - Use of the unreachable code check, which is enabled by default,
      would add `tag (line, column)` arguments on the `dzn lts` command
      line, making it more prone to hit the [arbitrary 8191 command-line limit](https://docs.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/command-line-string-limitation)) when running `dzn verify` on a Windows desktop.  Tags have
      been moved from the command-line to the verify pipeline; Note, the
      limit on Windows remains for other user input derived information
      communicated via the command-line.

For changes in the previous release see [Dezyne release 2.17.4](http://dezyne.org/dezyne-2.17.4-released.html).

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
