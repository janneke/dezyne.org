title: Dezyne 2.16.3 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2023-01-13 14:00:00 +0100
tags: Dezyne, Releases
slug: dezyne-2.16.3-released
summary: dezyne/ANNOUNCE-2.16.3
---
Dezyne 2.16.3 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.16.3.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.16.3.tar.gz)  
    [dezyne-2.16.3.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.16.3.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    42664fe34fcfe412fd9b5a049ec58de3a972793a  dezyne-2.16.3.tar.gz  
    c0af00b5dbaedb048efbea09f859ea9357d903dcd8b7f54a987828d82bbd24ac  dezyne-2.16.3.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.16.3 since 2.16.2
  * Noteworthy bug fixes
    - A bug was fixed in the C++ and C# code generators where a data
      parameter could shadow an event method.
    - Simulation of an interface with a subint event type now works
      correctly.
    - In the C++ runtime, a bug was fixed where a debug stream disabled
      with a nullptr streambuf could be used.
    - Using an enum or data type defined in an imported interface that is
      otherwise unused, is now supported.  As a consequence, code must be
      generated for all files now, i.e., also for files that only define
      types or use dollars.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
