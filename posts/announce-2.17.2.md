title: Dezyne 2.17.2 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2023-04-24 17:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.17.2-released
summary: dezyne/ANNOUNCE-2.17.2
---
Dezyne 2.17.2 is a bug-fix release.
Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.17.2.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.17.2.tar.gz)  
    [dezyne-2.17.2.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.17.2.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    cba66f6c26884afa77afbcf304653e390fde8451  dezyne-2.17.2.tar.gz  
    a3c2fcff862569943e19e195919c9cbd05e95d4b629974263382c50832b70aec  dezyne-2.17.2.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.17.2 since 2.17.1
  * Code
    - The experimental C code generator now supports out-parameters.
  * Commands
    - The different queue sizes: component, defer, external can now be set
      individually using `--queue-size`, `--queue-defer` and
      `--queue-size-external`.  The defaults are respectively: 3, 2 and 1.
  * Verification
    - The error on failing the determinism check is no longer ambiguous by
      removing "due to overlapping guards" from the message.
  * Simulate
    - The error on failing the determinism check is no longer ambiguous by
      removing "due to overlapping guards" from the message.
  * Noteworthy bug fixes
    - A bug was fixed in the type name lookup of a formal parameter
      binding (a <- b).
    - A bug was fixed in generalized integer expressions of form `a-b-c`.
    - A bug was fixed in the normalization involving implicit temporaries
      that would crash the verifier on constructs like g (f ($0$)).
    - A bug was fixed in the verifier that could lead to a false positive
      for models that have a blocking skip race.
    - The verifier no longer hangs attempting to create a witness for a
      livelock in a non-deterministic interface.
    - The well-formedness check now reports declarative statements inside
      a function as being incorrect.
    - A bug was fixed in the verifier for reporting queue-full cases for
      an external port.  As a result, queue-full is now reported during
      the illegal check and not as part of the deadlock check.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
