title: Dezyne 2.15.3 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-07-01 09:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.15.3-released
summary: dezyne/ANNOUNCE-2.15.3
---
Dezyne 2.15.3 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.15.3.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.15.3.tar.gz)  
    [dezyne-2.15.3.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.15.3.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    e7d1cdebd70f100e38b132c65bfbf37ac8689f89  dezyne-2.15.3.tar.gz  
    20871366a4f864c9d6e1f195aff4e91afcd7bd0969de5d8c21045e6fd833ff13  dezyne-2.15.3.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the 'gpg --verify' command.

# NEWS

### Changes in 2.15.3 since 2.15.2
  * Noteworthy bug fixes
    - A bug in the C++, C#, and the experimental Scheme and JavaScript
      runtime and code-generators was fixed for the use of handwritten
      foreign components without skeleton support (this is not
      recommended!).  This was a regression in 2.15.0 and for the
      experimental code generators in 2.15.2.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like IDE support based on the LSP (Language Server Protocol),
interactive integrated graphics, interactive simulation, (custom) code
generation and (custom) runtime library support.
