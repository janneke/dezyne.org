title: Dezyne 2.16.1 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-09-13 16:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.16.1-released
summary: dezyne/ANNOUNCE-2.16.1
---
Dezyne 2.16.1 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.16.1.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.16.1.tar.gz)  
    [dezyne-2.16.1.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.16.1.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    eb72ddc59094282da087e7417fa4c7e69de45e41  dezyne-2.16.1.tar.gz  
    41988bb5a264ec763f8be9ca2e37dd995242ddfa1b7801a2cbfe875cf7637324  dezyne-2.16.1.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.16.1 since 2.16.0
  * Code
    - In the the C++ runtime, the signature of skip_block has been changed
      and moved into the call_helper.
    - The C# code generator now supports using a requires injected port in
      a foreign component.
  * Verification
    - A performance regression since 2.15.4 for certain types of model was
      fixed.
  * Noteworthy bug fixes
    - A regression was fixed in the C++ and C# code generators that could
      make a valued blocking trigger not block if an earlier non-blocking
      reply was issued on the same port.
    - A regression was fixed in the C++ code generator for foreign
      components using a requires injected port.
    - The simulator now correctly shows a compliance error for a blocked
      trigger that is (marked) illegal in the provides interface.
    - The C++ runtime now avoids a compiler warning when using -DNDEBUG.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
