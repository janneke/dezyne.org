title: Dezyne 2.17.7 released.
author: Rutger van Beusekom, Janneke Nieuwenhuizen
date: 2023-10-30 10:00:00 +0100
tags: Dezyne, Releases
slug: dezyne-2.17.7-released
summary: dezyne/ANNOUNCE-2.17.7
---
Dezyne 2.17.7 is a bug-fix release.
Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.17.7.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.17.7.tar.gz)  
    [dezyne-2.17.7.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.17.7.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    161476eeb5def52bacef69fe6fee9698f652e050  dezyne-2.17.7.tar.gz  
    de9e4e2f2b90c159e77e74d7984240090bc966b49dba38b6331f55f804cd8227  dezyne-2.17.7.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

# NEWS
### Changes in 2.17.7 since 2.17.6
  * Noteworthy bug fixes
    - Building the C++ pump using the Boost::Coroutine library works out
      of the box again.  This was a regression introduced in 2.16.0.
    - The well-formedness check now allows using an early return in a
      tail-recursive function.
    - The well-formedness check now allows statements after a
      non-recursive call in a recursive function.
    - A bug leading to stray verification errors was fixed for two
      triggers using the same imperative statement.
    - The simulator now correctly reports a queue-full error for a
      non-compliant component where an unbounded defer is only restricted
      by its interface constraint.
    - The C++ thread-pool now respects its resource dependencies by
      declaring members in the proper order to allow problem/error free
      destruction.

For changes in the previous release see [Dezyne release 2.17.6](http://dezyne.org/dezyne-2.17.6-released.html).

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
