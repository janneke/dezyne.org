title: Dezyne 2.17.4 released.
author: Rutger van Beusekom, Janneke Nieuwenhuizen
date: 2023-08-03 14:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.17.4-released
summary: dezyne/ANNOUNCE-2.17.4
---
Dezyne 2.17.4 is a bug-fix release.
Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.17.4.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.17.4.tar.gz)  
    [dezyne-2.17.4.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.17.4.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    a6cc45826e7ba0fb8d101a7fe5cc342b157dbf06  dezyne-2.17.4.tar.gz  
    10a0bd455f6d8f87bcb11114a28f528a7940647ba95c9ef9a0a7ebd1b3610c4f  dezyne-2.17.4.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.17.4 since 2.17.3
  * Verification
    - A new `-T,--timings` option showing detailed timings for the
      different stages like: parsing, normalization and mCRL2 has been
      backported from 2.18.
  * Code
    - Lambda capture lists explicitly refer to `this` since C++20
      deprecates the implicit capture for `=`.
  * Noteworthy bug fixes
    - A verification fix to avoid the erroneously reporting of a deadlock
      for non-determinstic interface constraints has been backported from
      2.18.
    - A verification fix to correctly report unreachable code when
      interface and component imperative statement locations overlap was
      backported from 2.18.
    - Running the parser in `-f,--fall-back` mode now shows a parse tree
      again; a regression introduced in 2.17.2.
    - The verification no longer reports a false positive defer-induced
      <queue-full> when using defer after a function call.
    - A fix for the name resolver that would lead to a quadratic
      performance impact when looking up an undefined type was backported
      from 2.18.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
