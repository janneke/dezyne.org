title: Hello, world!
date: 1970-01-01 00:00:00
tags: hello
summary: dezyne.org is Haunted!
---

Finally, dezyne.org has been [Haunted](http://haunt.dthompson.us)!

![haunt logo](images/haunt/logo.png)

Connect with us [#dezyne @freenode](irc://freenode.net/#dezyne).
