title: Dezyne 2.17.0 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2023-01-23 14:00:00 +0100
tags: Dezyne, Releases
slug: dezyne-2.17.0-released
summary: dezyne/ANNOUNCE-2.17.0
---
We are happy to announce Dezyne 2.17 which introduces implicit interface
constraints.

Before 2.17.0, for a component to be compliant with its provides
interface(s), implementing a component required meticulously specifying
the same behavior in the component as in the provides interface;
therefore the code from the interface is often repeated in the
component.

Now the provides interface(s) are implicitly applied as
a constraint on the component behavior.  This means that anything
disallowed by the interface, i.e., explicitly or implicitly marked as
`illegal`, is implicitly marked as `illegal` in the component
behavior.

Also in this release: An unreachable code check, generalized integer
expressions, and a full rewrite of the language lookup mechanism fixing
some long standing namespace bugs.

The dzn.async ports feature---which proved not to be composable---has
been removed.

See also the [Dezyne documentation](https://dezyne.org/documentation.html).

We will evaluate your reports and track them via the
[Gitlab dezyne-issues
project](https://gitlab.com/groups/dezyne/-/issues), see our [guide to writing
helpful bug reports](https://dezyne.org/bugreport).

What's next?

In the next releases we like to see shared interface state and
verification with system scope for automatically exploring possible
traces in a system.

Future

Looking beyond the next releases: Module-specifications and
data-interfaces.  Hierarchical behaviors.  Support for Module Based
Testing.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.17.0.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.17.0.tar.gz)  
    [dezyne-2.17.0.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.17.0.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    ca074ffe6ceab64c49d74af21ceee7f8a7adcc4f  dezyne-2.17.0.tar.gz  
    fa86bfecad7fe359091779e13a03ce03804a39a22ed47b97c31065dd81388076  dezyne-2.17.0.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.17.0 since 2.16.3
  * Language
    - A component's behavior is now implicitly constrained by the behavior
      of its provides interfaces.
    - Unary minus is now supported in integer expressions.
    - Integer expressions have been generalized: It is no longer necessary
      to split a longer expression or to use parameters.
    - Support for `dzn.async' ports has been removed.
  * Parser
    - A new transformation has been added: `tag-imperative-blocks'.
    - Reduced well-formedness checking overhead by about 50%.
    - The behavior of imported components is no longer checked for
      well-formedness.
    - Defining an event in an interface and not using it in its behavior is
      now reported as a well-formedness error.
  * Verification
    - The verifier now checks for unreachable code.
    - As a result of the unreachable code check, two identical on
      statements are now reported as having an overlapping guard.
    - The verifier has a new option `-C,--no-constraint' to disable
      the constraining fo a component's behavior.
    - The verifier has a new option '-D,--non-non-compliance' to report a
      deadlock error instead of a non-compliance error.
    - The verifier has a new option `-U,--no-unreachable' to disable the
      unreachable code check.
    - The verifier has a new option `--jitty' to run `lps2ts' using
      `--rewriter=jittyc' for large models.
  * Code
    - The performance of the code generators has been improved by skipping
      normalization of imported interfaces and components.
    - The generated C++ and its runtime now uses typed pointers instead of
      void pointers for component instances.
    - The C++ code generator now uses C++11 style enums, instead of struct
      scoped C style enums.
    - The return type of the enum to_string function has been changed from
      std::string to char const*.
    - The enum stream operator << has been templatized to allow
      overloading for character type and traits.
    - The C++ code generator no longer generates redundant enum string
      conversion functions.
    - The C++ code generator now places the foreign skeleton constructor
      into its own compilation unit.  This assures compilation with
      non-redundantly generated new style enums regardless of import
      ordering.
  * Noteworthy bug fixes
    - Some longstanding bugs have been fixed in relation to
      + Extending nested namespaces in imports,
      + Referencing partially named identifiers,
      by a full rewrite of the lookup algorithm.
    - The C++ code generator now supports imported foreign components.
    - The C# code generator now supports foreign components returning a
      enum that is defined in an interface.
    - The experimental C code generator no longer generates duplicate enum
      string conversion functions.
    - The experimental C code generator now supports a foreign component at the
      top of a system.
    - The well-formedness check no longer crashes on a binary expression
      with a data constant.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
