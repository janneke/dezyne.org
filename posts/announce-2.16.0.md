title: Dezyne 2.16.0 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-08-18 16:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.16.0-released
summary: dezyne/ANNOUNCE-2.16.0
---
We are happy to announce Dezyne 2.16 which introduces the `defer`
keyword: A new language concept for implementing an asynchronous
interface.  With defer, the basic semantics are complete.

Defer replaces dzn.async ports feature that cannot be used in systems
collateral blocking.  The use of dzn.async ports is now deprecated.

Also new in this release: Cleanups to the code generators and model to
model transformations.

See also the [Dezyne documentation](https://dezyne.org/documentation.html).

We will evaluate your reports and track them via the
[Gitlab dezyne-issues
project](https://gitlab.com/groups/dezyne/-/issues), see our [guide to writing
helpful bug reports](https://dezyne.org/bugreport).

# What's next?

In the next releases we like to see implicit interface constraints,
shared interface state and verification with system scope for
automatically exploring possible traces in a system.

# Future

Looking beyond the next releases: Module-specifications and
data-interfaces.  Hierarchical behaviors.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.16.0.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.16.0.tar.gz)  
    [dezyne-2.16.0.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.16.0.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    91e8d61cdd9333edbd2768c26c187c35ba7e8a6c  dezyne-2.16.0.tar.gz  
    6d41b2e671afc629610de8f7ed48dd1799bae08b17bd9455a3b9b684f69524f4  dezyne-2.16.0.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the `gpg --verify` command.

Alternatively, Dezyne can be installed using GNU Guix:  

    guix pull  
    guix install dezyne


# NEWS

### Changes in 2.16.0 since 2.15.4
  * Language
    - A new keyword `defer` has been introduced.
    - The `dzn.async` ports are now deprecated.
    - Complex boolean and integer expressions are now supported.
  * Build
    - The tests for the experimental Scheme code generator are now being
      compiled.
    - The tests for the experimental Scheme and JavaScript code generators
      now also execute out-of-the-box in a container.
  * Code
    - The C++, C#, and experimental Scheme code generators support `defer`.
    - The experimental Scheme code generator now also supports collateral
      blocking and thus has full blocking support.
    - The C++ and C# runtime now has a more elegant and efficient
      implementation for collateral blocking.
    - The code generators now produce expressions with `&&` and `||` using
      courtesy parentheses.  This avoids compiler warnings.
    - The code generators no longer produce unnecessarily parenthesized
      and complex expressions.  This also avoids CLANG compiler warnings.
    - The code generators now preserve the top level comment.
  * Commands
    - The `dzn` command has a new option: `-t,--transform=TRANS`.  This
      makes dzn->dzn transformation avaiable from the command line.
      + New transformations have been added: `add-determinism-temporaries`,
        `inline-functions`, `simplify-guard-expressions`, and
        `split-complex-expressions`.
      + The `add-explicit-temporaries` transformation now supports complex
        boolean and integer expressions ([#67](https://gitlab.com/dezyne/dezyne-issues/-/issues/67)).
  * Noteworthy bug fixes
    - Some warnings in the C++ runtime have been fixed and side-stepped.
    - The mCRL2 code generator now generates correct code for an unused
      assignment from an action or function call as only statement in a
      branch of an if-statement.
    - Shadowing of a variable in a branch of an if-statement is no longer
      rejected.
    - The simulator now correctly displays a V-fork compliance error in a
      blocking trace.
    - The simulator now correctly handles a trace with foreign provides
      triggers.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like Verum-Dezyne's IDE support based on the LSP (Language
Server Protocol), interactive integrated graphics, interactive
simulation, (custom) code generation and (custom) runtime library
support.
