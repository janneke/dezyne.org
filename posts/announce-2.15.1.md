title: Dezyne 2.15.1 released.
author: Rutger van Beusekom, Jan (janneke) Nieuwenhuizen
date: 2022-06-02 10:00:00 +0200
tags: Dezyne, Releases
slug: dezyne-2.15.1-released
summary: dezyne/ANNOUNCE-2.15.1
---
Dezyne 2.15.1 is a bug-fix release.

Enjoy!\
The Dezyne developers.

# Download

    git clone git://git.savannah.nongnu.org/dezyne.git

Here are the compressed sources and a GPG detached signature[*]:  
    [dezyne-2.15.1.tar.gz](https://dezyne.org/download/dezyne/dezyne-2.15.1.tar.gz)  
    [dezyne-2.15.1.tar.gz.sig](https://dezyne.org/download/dezyne/dezyne-2.15.1.tar.gz.sig)

Here are the SHA1 and SHA256 checksums:  

    bf946bd8b13e55b055f67339c33796b86f10f4a1  dezyne-2.15.1.tar.gz  
    c657900fcc5a081b9aa8219a983f4b486d2830b8ff009fdd6190dcdd93122d7a  dezyne-2.15.1.tar.gz

[*] Use a .sig file to verify that the corresponding file (without the
.sig suffix) is intact.  First, be sure to download both the .sig file
and the corresponding tarball.  Then, run a command like this:

    gpg --verify .sig

If that command fails because you don't have the required public key,
then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

and rerun the 'gpg --verify' command.

# NEWS

### Changes in 2.15.1 since 2.15.0
  * Build
    * Tests that produce unstable witnesses have been disabled so that the
      test-suite now passes with different builds of mCRL2 that, in case
      of multiple counter examples, leads to reporting one of them
      (pseudo) randomly.
  * Noteworthy bug fixes
    * Verification no longer erroneously reports a deadlock when using
      blocking in a synchronous context.
    * The simulator no longer prints the initiating port event arrow twice
      which would lead to an invalid split-arrows trace for certain
      compliance errors.
    * The simulator now synthesizes the missing port action in case of a
      compliance error, to produce a complete split-arrows trace.
    * The simulator now reports a second reply error at the return of a
      void event if a reply was already set.
    * The simulator now reports a deadlock when using a void reply in a
      non-blocking synchronous context.
    * The simulator no longer reports a deadlock when using a skip-block
      reply in a function.
    * The simulator now correctly postpones flushing in system context
      when using multiple out events on a single modeling event until all
      events are enqueued.

#### About Dezyne

Dezyne is a programming language and a set of tools to specify,
validate, verify, simulate, document, and implement concurrent control
software for embedded and cyber-physical systems.

The Dezyne language has formal semantics expressed in
[mCRL2](https://mcrl2.org) developed at the department of Mathematics
and Computer Science of the Eindhoven University of Technology
([TUE](https://tue.nl)).  Dezyne requires that every model is finite,
deterministic and free of deadlocks, livelocks, and contract
violations.  This is achieved by means of the language itself as well as
builtin verification through model checking.  This allows the
construction of complex systems by assembling independently verified
components.

Dezyne is free software, it is distributed under the terms of the [GNU
Affero General Public Licence](https://www.gnu.org/licenses/#AGPL)
version 3 or later.

#### About Verum

[Verum](https://verum.com), the organization behind the Dezyne
language, is committed to continuing to invest in the language for the
benefit of all its users.  Verum assists its customers and partners in
solving the software challenges of today and tomorrow, by offering
expert consultancy on the application of the Dezyne language and the
development and use of its tools, as well as on Verum's commercial
tools like IDE support based on the LSP (Language Server Protocol),
interactive integrated graphics, interactive simulation, (custom) code
generation and (custom) runtime library support.
