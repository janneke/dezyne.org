;;; .guix.scm -- Guix package definition

;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;; Copyright © 2018,2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of dezyne.org.
;;;
;;; dezyne.org is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; dezyne.org is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with dezyne.org.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; The text and images on this site are free culture works available
;;; under the Creative Commons Attribution-NoDerivs 4.0 license, see
;;; https://creativecommons.org/licenses/by-nd/4.0/

(use-modules (guix profiles)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages rsync)
             (gnu packages version-control))

(packages->manifest
 (list git
       glibc
       glibc-utf8-locales
       gnu-make
       guile-3.0
       guile-syntax-highlight
       haunt
       rsync))
